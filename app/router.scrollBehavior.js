export default function (to, from, savedPosition) {
  // return saved position if any otherwise go to top
  if (savedPosition) {
    return savedPosition
  }
  return { x: 0, y: 0 }
}
