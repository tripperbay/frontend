export default async function (context) {
  const app = context.app

  if (!app.$auth.loggedIn) {
    return
  }

  const strategies = ['facebook', 'google', 'twitter']

  if (strategies.includes(app.$auth.strategy.name)) {
    const token = app.$auth.getToken(app.$auth.strategy.name).substr(7)

    // this is the provider token so exchange with the backend token
    try {
      const url = `/api/auth/${app.$auth.strategy.name}/`
      const response = await app.$axios.post(url, { access_token: token })
      app.$auth.setToken('local', 'Token ' + response.data.key)

      await app.$auth.setStrategy('local')
      await app.$auth.fetchUser()
      app.$snack.success('Yeah! Welcome back!')
    } catch (error) {
      app.$auth.logout()
      app.$snack.error(error, 0)
    }
  }
}
