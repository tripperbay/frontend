import Vue from 'vue'
import VueNativeSock from 'vue-native-websocket'

export default function ({ store }) {
  Vue.use(VueNativeSock, process.env.WS_URL, {
    store,
    format: 'json',
    reconnection: true,
    reconnectionAttempts: 5,
    reconnectionDelay: 3000,
    passToStoreHandler (eventName, event) {
      if (!eventName.startsWith('SOCKET_')) { return }

      let method = 'commit'
      let target = eventName.toUpperCase()
      let msg = event

      if (this.format === 'json' && event.data) {
        msg = JSON.parse(event.data)
        if (msg.mutation) {
          target = [msg.namespace || '', msg.mutation].filter(e => !!e).join('/')
        } else if (msg.action) {
          method = 'dispatch'
          target = [msg.namespace || '', msg.action].filter(e => !!e).join('/')
        }
      }
      // console.log('[ws] received ' + target)
      this.store[method](target, msg.data)
    },
  })
}
