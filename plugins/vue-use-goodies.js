import InfiniteLoading from 'vue-infinite-loading/src/components/InfiniteLoading.vue'
import Vue from 'vue'
import VueTimeago from 'vue-timeago'

Vue.component('infinite-loading', InfiniteLoading)
Vue.use(VueTimeago, {
  name: 'timeago',
  locale: 'en',
})
