import Vue from 'vue'
import moment from 'moment'

Vue.filter('default', (value, defaultValue) => {
  return value || defaultValue
})
Vue.filter('prettyUrl', (url) => {
  url = url.replace('http://www.', '').replace('https://www.', '')
  const questionMark = url.indexOf('?')
  if (questionMark === -1) {
    return url
  }
  return url.slice(0, questionMark)
})
Vue.filter('truncateChars', (text, stop, clamp) => {
  return text.slice(0, stop) + (stop < text.length ? clamp || ' ...' : '')
})
Vue.filter('upper', (text) => {
  return text.toUpperCase()
})

// https://momentjs.com/docs/#/displaying/
Vue.filter('formatDate', (isoDateString, formatString) => {
  const date = moment(isoDateString)
  return date.format(formatString)
})
