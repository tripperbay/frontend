import Vue from 'vue'

export default function ({ app, store }) {
  // inject $snack functions
  const defaultTimeout = 2000
  Vue.prototype.$snack = {
    queue (text, color = null, timeout = defaultTimeout) {
      store.dispatch('snackbar/add', {
        text,
        timeout,
        color,
      })
    },
    // for other colors use directly queue
    info (text, timeout = defaultTimeout) {
      this.queue(text, 'info', timeout)
    },
    success (text, timeout = defaultTimeout) {
      this.queue(text, 'success', timeout)
    },
    warning (text, timeout = defaultTimeout) {
      this.queue(text, 'warning', timeout)
    },
    error (text, timeout = defaultTimeout) {
      this.queue(text, 'error', timeout)
    },
  }
  app.$snack = Vue.prototype.$snack
}
