FROM node:13.8.0-alpine

RUN apk add --no-cache make gcc g++ python && \
    mkdir -p /tripperbay/frontend && \
    mkdir -p /tripperbay/media/static && \
    mkdir -p /tripperbay/media/uploads

WORKDIR /tripperbay/frontend
COPY . /tripperbay/frontend

# Install, build and remove O deps
RUN npm install && npm run build && apk del make gcc g++ python
