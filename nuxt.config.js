import colors from 'vuetify/es5/util/colors'
import webpack from 'webpack'

let envFileName = process.env.NODE_ENV == 'production' ? '.prod.env' : '.dev.env'

export default {
  debug: true,
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: 'TripperBay',
    title: 'TripperBay',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }, // TODO
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Lobster+Two:700&display=swap' }
    ],
    noscript: [{ innerHTML: "JavaScript Required. We're sorry, but TripperBay doesn't work properly without JavaScript enabled." }],
  },
  loading: { color: '#fff' },
  css: [
  ],
  plugins: [
    { src: '~/plugins/vue-injects.js'},
    { src: '~/plugins/vue-tb-filters.js'},
    { src: '~/plugins/vue-use-goodies.js'},
    { src: '~/plugins/vue-websocket.client.js'},
  ],
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify'
  ],
  modules: [
    '@nuxtjs/auth',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    'cookie-universal-nuxt',
    ["@nuxtjs/dotenv", {filename: envFileName}],
  ],
  auth: {
    plugins: [ '~/plugins/auth-social-to-local.js' ],
    strategies: {
      local: {
        endpoints: {
          login: { url: '/api/auth/login/', method: 'post', propertyName: 'key' },
          logout: { url: '/api/auth/logout/', method: 'post' },
          user: { url: '/api/main/account/', method: 'get', propertyName: false }
        },
        tokenRequired: true,
        tokenType: 'Token'
      },
      facebook: {
        client_id: '203776144153383',
        userinfo_endpoint: false,
        scope: ['public_profile', 'email', 'user_birthday']
      },
      google: {
        client_id: '380129894711-lij2jvjuqd30sm5m16csolgqs0lugnd8.apps.googleusercontent.com',
        userinfo_endpoint: false,
        scope: ['openid', 'profile', 'email']
      },
    }
  },
  axios: {
    // XXX although advertised as default (https://axios.nuxtjs.org/options#baseurl)
    // if not set below, the base url will be http://localhost:3000 on production builds
    baseURL: process.env.API_URL,
  },
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      // dark: true,
      themes: {
        dark: {
          // primary: colors.blue.darken2,
          primary: '#4f61ff',
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: '#4f61ff',
        }
      }
    }
  },
  build: {
    extend (config, ctx) {
    },
    plugins: [
      // global modules
      new webpack.ProvidePlugin({
        '_': 'lodash'
      })
    ],
    html: {
      minify: {
        collapseWhitespace: true,
        removeComments: true,
      },
    },
  },
  router: {
    middleware: ['sticky-next'],
  },
  server: {
    port: process.env.NUXT_PORT,
    host: process.env.NUXT_HOST,
  },
  pwa: {
    icon: {
      iconSrc: '~/static/icon.png',
    },
    manifest: {
      name: 'TripperBay',
      short_name: 'TripperBay',
      description: 'TripperBay Descr',
    },
    workbox: {
      // dev: true,
    },
  },
}
