export default function ({ from, route, redirect, store }) {
  if (!process.browser) {
    return
  }
  // just place next into local storage
  if (route.query.next) {
    window.localStorage.setItem('__next', route.query.next)
  }
}
