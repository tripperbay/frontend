export default function ({ from, route, redirect, store }) {
  if (!store.state.auth.loggedIn) {
    redirect('/')
  }

  if (!store.state.auth.user.account.user.is_superuser) {
    redirect('/')
  }
}
