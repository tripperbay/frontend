# TripperBay Nuxt.js Frontend

### Quickstart

Install the dependencies, export the environment variables and start with hot reload at http://127.0.0.1:3000

    npm install
    export $(cat example.env | xargs)
    npm run dev

### Build for production

Build for production and start the production server

    npm run build
    npm run start

## Generate static project

https://nuxtjs.org/guide#static-generated-pre-rendering-

    npm run generate

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
