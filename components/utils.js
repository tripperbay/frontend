export function packArray (array, count) {
  if (array.length <= count) {
    return array
  }

  const sliced = array.slice(0, count)
  sliced.push({ packed: array.length - count })
  return sliced
}
