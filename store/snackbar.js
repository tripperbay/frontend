export const state = () => ({
  queue: [],
  queueTrigger: null,
})

export const mutations = {
  QUEUE_ADD (state, snack) {
    state.queue.push(snack)
  },
  QUEUE_TRIGGER (state) {
    state.queueTrigger = Math.random()
  },
  QUEUE_POP_FIRST (state) {
    state.queue = _.filter(state.queue, (item, index) => {
      return index !== 0
    })
  },
}

export const actions = {
  add ({ state, commit }, snack) {
    commit('QUEUE_ADD', snack)
    commit('QUEUE_TRIGGER')
  },
}

export const getters = {
  queueIsEmpty (state) {
    return state.queue.length === 0
  },
}
