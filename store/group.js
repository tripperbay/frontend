export const state = () => ({
  object: null,
  members: {},
  proposalRideChoices: {},
  stream: { // this gets reset on group-stream mount based on route id
    next: null,
    results: [],
  },
  watcher: { // for triggering watchers on components
    proposalStayOverview: null,
    proposalRideOverview: null,
  },
})

export const mutations = {
  SET_STATE (state, { key, value }) {
    state[key] = value
  },
  WATCHER_TRIGGER (state, component) {
    state.watcher[component] = Math.random()
  },
  STREAM_INFINITE (state, data) {
    state.stream.results.push(...data.results)
    state.stream.next = data.next
  },
  STREAM_CREATE_ACTION (state, action) {
    state.stream.results.unshift(action)
  },
  STREAM_DELETE_ACTION (state, index) {
    state.stream.results = _.filter(state.stream.results, (action, i) => {
      return index !== i
    })
  },
  STREAM_UPDATE_VOTES (state, data) {
    const action = state.stream.results[data.index]
    action.action_object.votes_score = data.votes_score
    action.action_object.upvotes = data.upvotes
    action.action_object.downvotes = data.downvotes
  },
  MEMBERSHIP_CREATE (state, membership) {
    const member = {}
    const key = membership.account.id
    member[key] = membership

    state.members = {
      ...member,
      ...state.members,
    }
  },
  MEMBERSHIP_UPDATE (state, member) {
    state.members[member.account.id] = member
  },
  MEMBERSHIP_DELETE (state, accountId) {
    state.members = _.omit(state.members, accountId)
  },
}

export const actions = {
  async fetchObject ({ commit }, groupId) {
    const response = await this.$axios.get(`/api/main/group/${groupId}/`)
    commit('SET_STATE', { key: 'object', value: response.data })
  },
  async fetchMembers ({ commit }, groupId) {
    const response = await this.$axios.get(`/api/main/group/${groupId}/membership/list/`)
    commit('SET_STATE', { key: 'members', value: response.data })
  },
  async fetchProposalRideChoices ({ commit }) {
    const response = await this.$axios.get('/api/main/proposal/ride/choices/')
    commit('SET_STATE', { key: 'proposalRideChoices', value: response.data })
  },
  streamActionCreated ({ state, commit }, { groupId, action }) {
    if (state.object.id !== groupId) {
      return false
    }
    commit('STREAM_CREATE_ACTION', action)

    if (action.action_object_content_type.model === 'proposalstay') {
      commit('WATCHER_TRIGGER', 'proposalStayOverview')
    }
    if (action.action_object_content_type.model === 'proposalride') {
      commit('WATCHER_TRIGGER', 'proposalRideOverview')
    }
  },
  streamActionDeleted ({ state, commit }, { id, contentType, groupId }) {
    if (state.object.id !== groupId) {
      return false
    }

    _.each(state.stream.results, (action, index) => {
      if (action.action_object_content_type.model === contentType &&
          action.action_object.id === id) {
        commit('STREAM_DELETE_ACTION', index)
        return false
      }
    })

    if (contentType === 'proposalstay') {
      commit('WATCHER_TRIGGER', 'proposalStayOverview')
    }
    if (contentType === 'proposalride') {
      commit('WATCHER_TRIGGER', 'proposalRideOverview')
    }
  },
  streamActionVoted ({ state, commit }, data) {
    if (state.object.id !== data.groupId) {
      return false
    }

    _.each(state.stream.results, (action, index) => {
      if (action.action_object.id === data.id &&
          action.action_object_content_type.model === data.content_type) {
        commit('STREAM_UPDATE_VOTES', { index, ...data })
        return false
      }
    })
  },
  membershipCreate ({ state, commit }, membership) {
    if (state.object && state.object.id === membership.group.id) {
      commit('MEMBERSHIP_CREATE', membership)
    }
  },
  membershipUpdate ({ state, commit }, membership) {
    if (state.object && state.object.id === membership.group.id) {
      commit('MEMBERSHIP_UPDATE', membership)
    }
  },
  membershipDelete ({ state, commit }, { accountId, groupId }) {
    if (state.object.id === groupId) {
      commit('MEMBERSHIP_DELETE', accountId)
    }
  },
}

export const getters = {
  membersApproved (state) {
    return _.filter(state.members, (member) => {
      return member.approved_at != null
    })
  },
  membersPending (state) {
    return _.filter(state.members, (member) => {
      return member.approved_at == null
    })
  },
  requestUserIsApprovedMember (state, getters, rootState) {
    if (!rootState.auth.loggedIn) {
      return false
    }
    return _.filter(getters.membersApproved, (m) => {
      return m.account.id === rootState.auth.user.account.id
    }).length !== 0
  },
  requestUserIsPendingMember (state, getters, rootState) {
    if (!rootState.auth.loggedIn) {
      return false
    }
    return _.filter(getters.membersPending, (m) => {
      return m.account.id === rootState.auth.user.account.id
    }).length !== 0
  },
}
