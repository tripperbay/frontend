export const state = () => ({
  isDark: false,
  socket: {
    is_connected: false,
    reconnect_error: false,
  },
  interests: [],
  currencies: [],
  notifications: {
    count_unread: 0,
    next: '/api/main/account/notification/list/',
    results: [],
  },
  groupReportReasons: [],
})

export const mutations = {
  SOCKET_ONOPEN (state) {
    state.socket.is_connected = true
  },
  SOCKET_ONCLOSE (state) {
    state.socket.is_connected = false
  },
  SOCKET_ONERROR (state, event) {
  },
  SOCKET_ONMESSAGE (state, message) {
    // pass
  },
  SOCKET_RECONNECT (state, count) {
    // pass
  },
  SOCKET_RECONNECT_ERROR (state) {
    state.socket.reconnect_error = true
  },
  SET_STATE (state, { key, value }) {
    state[key] = value
  },
  // INTEREST_UPDATE (state, { index, object, key, value }) {
  //   state.interests[index][key] = value
  // },
  // INTEREST_ADD_IMAGE (state, { index, image }) {
  //   state.interests[index].images.push(image)
  // },
  // INTEREST_TOGGLE_ACTIVE (state, index) {
  //   state.interests[index].is_active = !state.interests[index].is_active
  // },
  NOTIFICATIONS_SET_COUNT_UNREAD (state, count) {
    state.notifications.count_unread = count
  },
  NOTIFICATIONS_INFINITE (state, data) {
    state.notifications.results.push(...data.results)
    state.notifications.next = data.next
  },
  NOTIFICATION_CREATE (state, notification) {
    state.notifications.results.unshift(notification)
    state.notifications.count_unread += 1
  },
  NOTIFICATION_MARK_READ (state, index) {
    state.notifications.results[index].unread = false
  },
}

export const actions = {
  async fetchInterestList ({ commit }) {
    const response = await this.$axios.get('/api/main/interest/list/')
    commit('SET_STATE', { key: 'interests', value: response.data })
  },
  async fetchCurrencyList ({ commit }) {
    const response = await this.$axios.get('/api/main/currency/list/')
    commit('SET_STATE', { key: 'currencies', value: response.data })
  },
  async fetchGroupReportReasonList ({ commit }) {
    const response = await this.$axios.get('/api/main/group/report/reasons/')
    commit('SET_STATE', { key: 'groupReportReasons', value: response.data })
  },
  notificationCreate ({ state, commit }, notification) {
    commit('NOTIFICATION_CREATE', notification)
  },
  notificationMarkRead ({ state, commit }, notificationId) {
    _.each(state.notifications.results, (n, index) => {
      if (n.id === notificationId) {
        commit('NOTIFICATION_MARK_READ', index)
        return false
      }
    })
  },
  // updateInterest ({ state, commit }, { object, key, value }) {
  //   _.each(state.interests, (interest, index) => {
  //     if (interest.id === object.id) {
  //       commit('INTEREST_UPDATE', { index, object, key, value })
  //       return false
  //     }
  //   })
  // },
  // interestAddImage ({ state, commit }, { id, image }) {
  //   _.each(state.interests, (interest, index) => {
  //     if (interest.id === id) {
  //       commit('INTEREST_ADD_IMAGE', { index, image })
  //     }
  //   })
  // },
  // interestToggleActive ({ state, commit }, id) {
  //   _.each(state.interests, (interest, index) => {
  //     if (interest.id === id) {
  //       commit('INTEREST_TOGGLE_ACTIVE', index)
  //     }
  //   })
  // },
}
