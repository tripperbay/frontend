import moment from 'moment'

export const state = () => ({
  // this holds all upcoming trips
  objects: {
    count: null,
    next: '/api/main/explore/group/list/',
    results: [],
  },
  // start date filter
  date: {
    start: moment().add(1, 'days').format('YYYY-MM-DD'), // default start in backend
  },
})

export const mutations = {
  SET_STATE (state, { key, value }) {
    state[key] = value
  },
  OBJECTS_INFINITE (state, data) {
    state.objects.results.push(...data.results)
    state.objects.next = data.next

    if (!state.objects.count) {
      state.objects.count = data.count
    }
  },
  OBJECT_CREATE (state, group) {
    state.objects.results.push(group)
  },
}

export const actions = {
  setStartDate ({ commit }, date) {
    const value = {
      count: null,
      next: `/api/main/explore/group/list/?starts_at__gte=${date}`,
      results: [],
    }
    commit('SET_STATE', { key: 'objects', value })
    commit('SET_STATE', { key: 'date', value: { start: date } })
  },
}
