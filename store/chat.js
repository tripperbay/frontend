export const state = () => ({
  rooms: {
    count_unread: 0,
    next: '/api/chat/room/list/',
    results: [],
  },
  room: null, // the selected room set on ROOM_ENTER
  room_account_typing: null, // the account whos typing in selected room
  messages: { // messages in a selected room
    count_unread: 0,
    first_new_id: null,
    next: null,
    results: [],
  },
  watcher: { // used to trigger stuff on the component watcher
    scroll: null, // applies a random number so scrolling is triggered in the watcher
    message_seen: null, // applies a message dict so ajax is triggered in the watcher
  },
})

export const mutations = {
  ROOMS_CLEAR (state) {
    state.rooms.next = '/api/chat/room/list/'
    state.rooms.results = []
  },
  ROOM_CLEAR (state) {
    state.room = null
    state.room_account_typing = null
    state.messages = {
      count_unread: 0,
      first_new_id: null,
      next: null,
      results: [],
    }
    state.watcher = {
      scroll: null,
      message_seen: null,
    }
  },
  ROOMS_INFINITE (state, data) {
    state.rooms.results.push(...data.results)
    state.rooms.next = data.next
  },
  ROOM_ENTER (state, room) {
    state.room = room
    state.room_account_typing = null

    state.messages.count_unread = 0
    state.messages.next = room.url_message_list
    state.messages.results = []
  },
  ROOM_CREATE (state, room) {
    state.rooms.results.unshift(room)
  },
  ROOMS_SET_COUNT_UNREAD (state, count) {
    state.rooms.count_unread = count
  },
  ROOMS_INCREMENT_COUNT_UNREAD (state) {
    state.rooms.count_unread += 1
  },
  ROOM_INCREMENT_COUNT_UNREAD (state, index) {
    state.rooms.results[index].count_unread += 1
  },
  ROOMS_SET_LAST_MESSAGE (state, { message, index }) {
    state.rooms.results[index].last_message = message
  },
  ROOMS_SORT_BY_LAST_MESSAGE (state) {
    state.rooms.results = _.sortBy(state.rooms.results, (room) => {
      if (room.last_message == null) {
        return new Date(1970)
      }
      return new Date(room.last_message.created_at)
    }).reverse()
  },
  ROOM_MARK_AS_SEEN (state) {
    _.each(state.rooms.results, (room) => {
      if (room.id === state.room.id) {
        state.rooms.count_unread -= room.count_unread
        room.count_unread = 0
        return false
      }
    })
  },
  MESSAGES_INFINITE (state, data) {
    state.messages.next = data.next
    state.messages.results.unshift(...data.results.reverse())
  },
  MESSAGE_CREATE (state, message) {
    state.messages.results.push(message)
    state.watcher.scroll = Math.random()
  },
  MESSAGE_CREATE_CONFIRM_RECEIPT (state, { oldId, message }) {
    _.each(state.messages.results, (result) => {
      if (result.id === oldId) {
        result.id = message.id
        result.created_at = message.created_at
        return false
      }
    })
  },
  MESSAGE_SET_SEEN_BY (state, message) {
    _.each(state.messages.results, (result) => {
      if (result.id === message.id) {
        result.seen_by = message.seen_by
        return false
      }
    })
  },
  // MESSAGES_INCREMENT_COUNT_UNREAD (state) {
  //   state.messages.count_unread += 1
  // },
  // MESSAGES_SET_FIRST_NEW_ID (state, message_id) {
  //   state.messages.first_new_id = message_id
  // },
  ROOM_ACCOUNT_TYPING_SET (state, account) {
    state.room_account_typing = account
    state.watcher.scroll = Math.random()
  },
  WATCHER_MESSAGE_SEEN (state, message) {
    state.watcher.message_seen = message
  },
}

export const actions = {
  async fetchRoom ({ state, commit, dispatch }, hashId) {
    try {
      const response = await this.$axios.get(`/api/chat/room/${hashId}/`)
      commit('ROOM_ENTER', response.data)
    } catch {
      this.$router.push('/')
    }
  },
  messageSend ({ state, commit }, message) {
    if (state.room.id === message.room_id) {
      commit('MESSAGE_CREATE', message)
    }
  },
  messageSendConfirmReceipt ({ state, commit }, { oldId, message }) {
    if (state.room.id === message.room_id) {
      commit('MESSAGE_CREATE_CONFIRM_RECEIPT', { oldId, message })
    }
  },
  message_received ({ state, commit, dispatch, rootState }, message) {
    _.each(state.rooms.results, (room, index) => {
      if (room.id === message.room_id) {
        commit('ROOMS_SET_LAST_MESSAGE', { message, index })
        commit('ROOMS_SORT_BY_LAST_MESSAGE')
        return false
      }
    })

    // stop if this is the user who sent the message
    if (message.created_by.id === rootState.auth.user.account.id) {
      return
    }

    if (state.room != null && state.room.id === message.room_id) {
      commit('ROOM_ACCOUNT_TYPING_SET', null)
      commit('MESSAGE_CREATE', message)
      commit('WATCHER_MESSAGE_SEEN', message)

      // commit('MESSAGES_INCREMENT_COUNT_UNREAD')
      // if (state.messages.count_unread == 1){
      //   commit('MESSAGES_SET_FIRST_NEW_ID', message.id)
      // }
    } else {
      commit('ROOMS_INCREMENT_COUNT_UNREAD')
      _.each(state.rooms.results, (room, index) => {
        if (room.id === message.room_id) {
          commit('ROOM_INCREMENT_COUNT_UNREAD', index)
          return false
        }
      })
    }
  },
  message_typing ({ state, commit }, { account, roomId }) {
    if (state.room == null) {
      return false
    }
    if (roomId !== state.room.id) {
      return false
    }
    if (account == null && state.room_account_typing == null) {
      // already cleared by message_received
      return false
    }
    commit('ROOM_ACCOUNT_TYPING_SET', account)
  },
}
